(function(){
	var app = new Vue({
		el: "#exemplo",
		data: {
			titulo: "Formulario",
			nome: "",
			itens: [],
			log: false
		},
		methods:{
			clicou: function(){
				alert("Clicou");
			},
			addItem: function(){
				var item = this.nome;
				this.itens.push(item);
				this.nome = "";
			},
			removerItem: function(item){
				this.itens.$remove(item);
			},
			showLog: function(){
				this.log = !this.log;
			}
		}
	})
})()