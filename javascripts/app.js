(function(){

	var appView = new Vue({
		el: "#app",
		data: {
			usuario: {
				email: "matheuslucenam@gmail.com"
			},
			jsLinks:[
				{link: "https://jquery.com/", nome: "JQuery"},
				{link: "https://bower.io/", nome: "Bower"},
				{link: "https://nodejs.org/en/", nome: "Nodejs"},
				{link: "http://electron.atom.io/", nome: "Electron"},
				{link: "https://vuejs.org/", nome: "Vue.js"},
			],
			cssLinks:[
				{link: "http://getbootstrap.com/", nome: "Bootstrap"},
				{link: "https://wrapbootstrap.com/", nome: "Wrapbootstrap"},
				{link: "http://tympanus.net/codrops/", nome: "Codrops"}
			],
			livrosLinks:[
				{link: "https://www.google.com.br/#q=segredos+do+ninja+javascript", nome: "Segredos do ninja javascript"},
				{link: "https://www.google.com.br/#q=padr%C3%B5es+javascript", nome: "Padroes javascript"},
				{link: "https://www.google.com.br/#q=jquery+a+biblioteca+do+programador+javascript", nome: "Jquery a biblioteca do programador javascript"}
			],
			animacao: false,
			mostrarResposta: false,
			tarefas: [],
			tarefa: {descricao: '', concluida: false}
		},
		computed:{
			tarefasConcluidas: function(){
				return _.filter(this.tarefas, ['concluida', true]);
			},
			tarefasIncompletas: function(){
				return _.filter(this.tarefas, ['concluida', false]);
			}
		},
		methods: {
			toggleMenuLateral: function(){
				$("body").toggleClass("menu-lateral-toggled");
			},
			toggleLimites: function(){
				$("body").toggleClass("limites");
			},
			animacaoToggle: function(){
				this.animacao=!this.animacao;
			},
			btnMostrarResposta: function(){
				this.mostrarResposta=!this.mostrarResposta;
			},
			addTarefa: function(){
				this.tarefas.push(Vue.util.extend({}, this.tarefa));
				this.reset();
			},
			removerTarefa: function(tarefa){
				this.tarefas.$remove(tarefa);
			},
			concluirTarefa: function(tarefa){
				tarefa.concluida = true;
			},
			removerConcluidas: function(){
				var vm = this;
				_.forEach(this.tarefasConcluidas, function(t) {
				  vm.removerTarefa(t);
				});
			},
			reset: function(){
				this.tarefa = {descricao: '', concluida: false};
			}
		}
	})

})();